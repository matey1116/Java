import java.util.*;
import java.text.*;
public class Average{
  private static double averageScoreNormal;
  private static double averageScoreChanged;
  private static ThreeDiceScorer threeDiceScorer;

  public static void main(String[] args) {
    //calling the functions to compute the average
    computeAverageNormal();
    computeAverageChanged();
  }

  public static void computeAverageNormal(){
    int totalScore = 0;
    //using 3 for loops to generate all 216 possible combinations for 3 fair dice
    for (int die1 = 1; die1 < 7; die1++){
      for (int die2 = 1; die2 < 7; die2++){
        for (int die3 = 1; die3 < 7; die3++){
          //passing the dice to the ThreeDiceScorer to compute the score
          threeDiceScorer = new ThreeDiceScorer(die1, die2, die3);
          //adding the score to the total pool
          totalScore = totalScore + threeDiceScorer.getPlayerScore();
        }
      }
    }

    //printing what is the average amount of points for 3 fair 6 side dice
    averageScoreNormal = 1.0 * totalScore / 216;
    DecimalFormat df = new DecimalFormat("#.###");

    System.out.println("The average score of 3 normal dice is: " + df.format(averageScoreNormal));
  }

  public static void computeAverageChanged(){
    int totalScore = 0;
    //using 3 for loops to generate all 216 possible combinations for 3 fair dice
    for (int die1 = 1; die1 < 7; die1++){
      for (int die2 = 1; die2 < 7; die2++){
        for (int die3 = 1; die3 < 7; die3++){
          //because 1 of the dice has 2,3,4,5,6,6, if the number is 1 the program adds 5 to it to compute it correctly based on the requirements
          if(die3 == 1){
            threeDiceScorer = new ThreeDiceScorer(die1, die2, (die3 + 5));
          }
          else{
          threeDiceScorer = new ThreeDiceScorer(die1, die2, die3);

        }
		totalScore = totalScore + threeDiceScorer.getPlayerScore();
        }
      }
    }
        //printing what is the average amount of points for 3 fair 6 side dice one of which has 2,3,4,5,6,6
        averageScoreChanged = 1.0 * totalScore / 216;
    DecimalFormat df = new DecimalFormat("#.###");
    double difference = (averageScoreChanged - averageScoreNormal);
    System.out.println("The average score of 2 normal dice and 1 changed is: " + df.format(averageScoreChanged));
    System.out.println("The difference between 3 normal dice and 2 normal and 1 changed dice is " + df.format(difference));
  }
}
