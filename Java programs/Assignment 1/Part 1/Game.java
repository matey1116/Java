import java.util.*;
import java.text.*;
public class Game{
  //declaring game variables to store information in
  public static int numberOfRounds;
  public static Scanner input = new Scanner(System.in);
  public static int[] player1Array = new int[3];
  public static int[] player2Array = new int[3];
  public static ThreeDiceScorer player1Dice;
  public static ThreeDiceScorer player2Dice;
  public static int player1WinCount = 0;
  public static int player2WinCount = 0;
  public static long player1TotalPoints = 0;
  public static long player2TotalPoints = 0;

  public static void main(String[] args){
    System.out.print("Please input the number of round to play (min 0): ");
    //calling the two methods to start the game but first to input the variables
    roundsInput();
    gameStart();
  }

  public static void roundsInput(){
    //prompt the user to input an integer
    numberOfRounds = input.nextInt();
    
    //if number is below 0 make a recursive call to the same method
    if (numberOfRounds < 0){
        System.out.print("Please enter a number that is not negative: ");
        roundsInput();
    }
  }

  public static void numberGenerator(){
    Random random = new Random();

    //generate 3 dice for each player
    for (int i = 0; i < 3; i++){
      player1Array[i] = (1 + random.nextInt(6));
      player2Array[i] = (1 + random.nextInt(6));
    }
  }

  public static void gameStart(){
    System.out.println();
    //loop the program the number of times the user has inputted
      for (int i = 1; i <= numberOfRounds; i++){

        //calling the method to generate the dice for the players
        numberGenerator();
        
        //passing the numbers to the ThreeDiceScorer to compute
        player1Dice = new ThreeDiceScorer(player1Array[0], player1Array[1], player1Array[2]);
        player2Dice = new ThreeDiceScorer(player2Array[0], player2Array[1], player2Array[2]);

        //printing each round's information for each player and their score
        System.out.print("Round " + i);
        System.out.print("   Player 1: " + player1Array[0] + " " + player1Array[1] + " " + player1Array[2]);
        System.out.print("   Points: " + player1Dice.getPlayerScore());
        System.out.print("   Player 2: " + player2Array[0] + " " + player2Array[1] + " " + player2Array[2]);
        System.out.print("   Points: " + player2Dice.getPlayerScore());

        //calling the method
        printWinner();

        //add the points of the current round for each player to their totalpoints
        player1TotalPoints = player1TotalPoints + player1Dice.getPlayerScore();
        player2TotalPoints = player2TotalPoints + player2Dice.getPlayerScore();
      }
      average();

    }
    public static void printWinner(){
      //check which of the players won the current round and count how many times each player has won
      if (player1Dice.getPlayerScore() > player2Dice.getPlayerScore()){
        System.out.println("   Round winnter is Player 1.");
        player1WinCount++;
      }
      else{
        if (player1Dice.getPlayerScore() < player2Dice.getPlayerScore()){
          System.out.println("  Round winnter is Player 2.");
          player2WinCount++;
        }
        else{
          System.out.println("   Round is tied!");
        }
      }
    }

    public static void average(){
      //declaring the decimal format for the final output
      DecimalFormat df = new DecimalFormat("#.###");

      //printing each player's win count and total points
      System.out.println();
      System.out.print("Total wins:   Player1: " + player1WinCount);
      System.out.println("   Player 2: " + player2WinCount);

      System.out.print("Total points:   Player 1: " + player1TotalPoints);
      System.out.println("   Player 2: " + player2TotalPoints);

      //calculating the average points for each player based on all rounds
      double player1Average = 1.0 * player1TotalPoints / numberOfRounds;
      double player2Average = 1.0 * player2TotalPoints / numberOfRounds;

      System.out.println();
      System.out.print("Average points per round:   Player 1: " + df.format(player1Average));
      System.out.println("   Player 2: " + df.format(player2Average));

      //printing which player won the game by overall points
      if (player1TotalPoints > player2TotalPoints){
        System.out.println("Overall points winner is Player 1.");
      }
      else{
        if (player1TotalPoints == player2TotalPoints){
          System.out.println("Both players have the same points. The game is a tie.");
        }
        else{
          System.out.println("Overall points winner is Player 2.");
        }
      }
    }
}
