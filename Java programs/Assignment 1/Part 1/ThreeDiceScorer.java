public class ThreeDiceScorer extends ThreeDice{
  private int playerScore = 0;

  public ThreeDiceScorer(int s1, int s2, int s3){
    //passing the variables to the super class and starting the computation
    super(s1, s2, s3);
    playerScore();
  }

  public int getPlayerScore(){
    return playerScore;
  }

  //computing how many points the player got
  public void playerScore(){
    playerScore = getDie1() + getDie2() + getDie3();

    if (threeSame()){
      playerScore = playerScore + 60;
    }

    else if (runOfThree()){
      playerScore = playerScore + 40;
    }

    else if (pair()){
      playerScore = playerScore + 20;
    }
  }
}
