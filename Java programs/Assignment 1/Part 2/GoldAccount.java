import java.text.*;
public class GoldAccount extends StandardAccount implements AccountsInterface{
  public GoldAccount(int dM, int nM, int mb){
     //passing the user inputted values and the account specific values from the interface to the super class
    super(dM, nM, mb, goldPackageCost, goldDayCost, goldNightCost, goldMbCost, goldNumOfChannels, goldBroadbandIncluded);
  }
  //abstract method to return the type of the account
  public String accountType(){
    return "Gold";
  }
}
