public interface AccountsInterface{
    //declaring variables to use by each account type
    //easy to edit afterwards if any prices change.
    int bronzePackageCost = 36;
    double bronzeDayCost = 0.12;
    double bronzeNightCost = 0.05;
    double bronzeMbCost = 0.02;
    int bronzeNumOfChannels = 60;
    int bronzeBroadbandIncluded = 500;
    
    int silverPackageCost = 46;
    double silverDayCost = 0.12;
    double silverNightCost = 0.00;
    double silverMbCost = 0.01;
    int silverNumOfChannels = 130;
    int silverBroadbandIncluded = 1000;

    int goldPackageCost = 61;
    double goldDayCost = 0.00;
    double goldNightCost = 0.00;
    double goldMbCost = 0.01;
    int goldNumOfChannels = 230;
    int goldBroadbandIncluded = 1520;
}