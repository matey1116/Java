import java.text.*;
public class SilverAccount extends StandardAccount implements AccountsInterface{
  public SilverAccount(int dM, int nM, int mb){
    //passing the user inputted values and the account specific values from the interface to the super class
    super(dM, nM, mb, silverPackageCost, silverDayCost, silverNightCost, silverMbCost, silverNumOfChannels, silverBroadbandIncluded);
  }

  //abstract method to return the type of the account to the super class
  public String accountType(){
    return "Silver";
  }


}
