import java.text.*;
public class BronzeAccount extends StandardAccount implements AccountsInterface{
  public BronzeAccount(int dM, int nM, int mb){
    //passing the user inputted values and the account specific values from the interface to the super class
    super(dM, nM, mb, bronzePackageCost, bronzeDayCost, bronzeNightCost, bronzeMbCost, bronzeNumOfChannels, bronzeBroadbandIncluded);
  }
  //abstract method to return the type of the account
  public String accountType(){
    return "Bronze";
  }

}
