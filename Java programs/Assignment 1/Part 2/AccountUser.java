import java.util.*;
public class AccountUser{
  //variable initialisation available to all methods
  private static Scanner input = new Scanner(System.in);
  private static int dayMinutes;
  private static int nightMinutes;
  private static int mbPerMonth;

  public static void main(String[] args) {
    //calling the input method
    input();
    //creating instances of all 3 types of accounts and passing the values which the user inputted
    BronzeAccount bronze = new BronzeAccount(dayMinutes, nightMinutes, mbPerMonth);
    bronze.print();

    SilverAccount silver = new SilverAccount(dayMinutes, nightMinutes, mbPerMonth);
    silver.print();

    GoldAccount gold = new GoldAccount(dayMinutes, nightMinutes, mbPerMonth);
    gold.print();

    //if statements to print which of the accounts is the cheapest and if 2 of them have the same price, show which one is better than the other
    if (bronze.getTotalCost() < silver.getTotalCost() && bronze.getTotalCost() < gold.getTotalCost()){
      System.out.println("\nBronze account is the cheapest");
    }
    else if (silver.getTotalCost() <bronze.getTotalCost() && silver.getTotalCost() < gold.getTotalCost()){
      System.out.println("\nSilver account is the cheapest");
    }
    else if (gold.getTotalCost() < bronze.getTotalCost() && gold.getTotalCost() < silver.getTotalCost()){
      System.out.println("\nGold account is the cheapest");
    }
    else if (bronze.getTotalCost() == silver.getTotalCost() && silver.getTotalCost() < gold.getTotalCost()){
      System.out.println("\nBronze and Silver have the same cost but with Silver you receive Spotify account");
    }
    else if (gold.getTotalCost() == silver.getTotalCost() && gold.getTotalCost() < bronze.getTotalCost()){
      System.out.println("\nSilver and Gold have the same cost but with Gold you receive Music on Demand");
    }

  }

  //method for inputting the values. If the value the user inputted is negative, the while loop will loop until the user
  //inputs a value that is positive or 0
  public static void input(){
    System.out.print("Please enter the number of daytime minutes used per month: ");
    dayMinutes = input.nextInt();

    while(dayMinutes < 0){
      System.out.print("Please enter the number of daytime minutes used per month: ");
      dayMinutes = input.nextInt();
    }

    System.out.print("Please enter the number of nighttime minutes used per month: ");
    nightMinutes = input.nextInt();

    while(nightMinutes < 0){
      System.out.print("Please enter the number of nighttime minutes used per month:  ");
      nightMinutes = input.nextInt();
    }

    System.out.print("Please enter the number of Megabytes used per month: ");
    mbPerMonth = input.nextInt();
    
    while(mbPerMonth < 0){
      System.out.print("Please enter the number of Megabytes used per month: ");
      mbPerMonth = input.nextInt();
    }
  }
}
