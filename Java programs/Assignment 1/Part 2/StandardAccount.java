import java.text.*;
public abstract class StandardAccount{
  //Declaring protected variables for the classes in the hierarchy
  protected int dayMinutes;
  protected int nightMinutes;
  protected int mbPerMonth;
  protected int packageCost;
  protected double dayCost;
  protected double nightCost;
  protected double mbCost;
  protected int numOfChannels;
  protected int broadbandIncluded;
  protected double totalDayCost;
  protected double totalNightCost;
  protected double totalMbCost;
  protected double totalCost;
  protected int mbExtra;
  protected final String pound = "\u00a3";
  protected final String gold = "Music on Demand provided";
  protected final String silver = "Spotify Acoount provided";

  //constructor method to save the variables that are passed to the local variables
  public StandardAccount(int dayMinutes, int nightMinutes, int mbPerMonth, int packageCost, double dayCost, double nightCost, double mbCost, int numOfChannels, int broadbandIncluded){
    this.dayMinutes = dayMinutes;
    this.nightMinutes = nightMinutes;
    this.mbPerMonth = mbPerMonth;
    this.packageCost = packageCost;
    this.dayCost = dayCost;
    this.nightCost = nightCost;
    this.mbCost = mbCost;
    this.numOfChannels = numOfChannels;
    this.broadbandIncluded = broadbandIncluded;
  }
  //declaring the abstract method so the account types can return the correct account name
  public abstract String accountType();

  //returning the total cost of the account that is being computed
  public double getTotalCost(){
    return totalCost;
  }

  //method for computing minute cost per month and mb cost per month based on the values that the user inputted
  public void compute(){

    //checking if the user usage of mb is higher than the broadband included. If the user doesn't use more broadband than provided,
    //the extra megabytes is 0. Otherwise, returns the absolute value of the extra megabytes
    if ((broadbandIncluded - mbPerMonth) > 0){
      mbExtra = 0;
    }
    else{
      mbExtra = Math.abs(broadbandIncluded - mbPerMonth);
    }

    //computing the total cost of day, night and mb cost
    totalDayCost = (1.0 * dayMinutes * dayCost);
    totalNightCost = (1.0 * nightMinutes * nightCost);

    totalMbCost = (1.0 * mbExtra * mbCost);
    //summing day, night, mb and account cost for total cost
    totalCost = (totalMbCost + totalNightCost + totalDayCost + packageCost);
  }

  public void print(){
    //calling the compute method to calculate the cost of each part of the account
    compute();

    //declaring decimal format for the double variables
    DecimalFormat df = new DecimalFormat("#.##");

    //Printing all the information for each account
    System.out.println("\nAccount Summary for " + accountType() + " Account"); //prints the name of the account using the abstract method declared
    System.out.println("Package Cost: " + pound + packageCost);
    System.out.println("Cost of daytime calls: " + pound + dayCost + "/min");
    System.out.println("Cost of evening and weekend calls: " + pound + nightCost + "/min");
    System.out.println("Number of Channels: " + numOfChannels);
    System.out.println("Broadband Included: " + broadbandIncluded + "mb");
    System.out.println("Broadband Cost (above included limit): " + pound + mbCost + "/Mb");
    System.out.println("Total daytime calls cost: " +pound + df.format(totalDayCost));
    System.out.println("Total evening calls cost: " +pound + df.format(totalNightCost));
    System.out.println("Total (extra) broadband cost: " +pound + df.format(totalMbCost));
    System.out.println("Total cost: " + pound + df.format(totalCost));

    //prints extra benefits of each account
    if (accountType() == "Gold" ){
      System.out.println(silver);
      System.out.println(gold);
    }
    else{
      if (accountType() == "Silver"){
        System.out.println(silver);
      }
    }

  }
}
