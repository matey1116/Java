import java.util.*;
public class BreakCaesar{

    public static void main(String[] args) {
        try{
            Decryption decrypt = new Decryption(args[0]);
            decrypt.decrypt();
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("\nPlease enter the encrypted text. \nIf the text has spaces, put quatation marks.");
            System.exit(1);
        }
    }
}
