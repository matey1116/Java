import java.util.*;
public class Decryption{
  protected double[] knownFreq = {0.0855, 0.0160, 0.0316, 0.0387, 0.1210, 0.0218, 0.0209, 0.0496, 0.0733, 0.0022, 0.0081, 0.0421, 0.0253, 0.0717, 0.0747, 0.0207, 0.0010, 0.0633, 0.0673, 0.0894, 0.0268, 0.0106, 0.0183, 0.0019, 0.0172, 0.0011};
  protected HashMap<Character, Double> englishFrequency = new HashMap<Character, Double>();
  protected HashMap<Character, Double> encryptedFrequency = new HashMap<Character, Double>();
  protected double[] chi = new double[26];
  protected String encrypted;
  protected String decrypted;
  protected char[] chars ;

  public Decryption(String str){
    encrypted = str;
  }

  public void decrypt(){
    chars = encrypted.toCharArray();

    frequencyCompute(encrypted);
    chiSquare();
    findSmallest();
    print();
  }
  //funciton to create a hashmap with each english letter and its coresponding frequency
  protected void frequencyCompute(String s){
      s = s.replaceAll("[^a-zA-Z0-9]", "");
      //english letter frequency
      for (char ch = 'a'; ch <= 'z'; ch++) {
          int pos = ch - 'a';
          englishFrequency.put(ch, knownFreq[pos]);
          encryptedFrequency.put(ch, 0.0);
      }
  }

  //function to compue the chi values for each possible encryption
  protected void chiSquare(){

      //loop 26 times for each possible shift in the english alphabet
      for (int shift = 0; shift < 26; shift++){
          //remove all non alphabet characters
          String str = encrypted.replaceAll("[^a-zA-Z0-9]", "");
          //make all letters lowercase in order the computation to be able to compute them
          str = str.toLowerCase();

          for(int j = 0; j < str.length(); j++){
              //shift each letter from the encrypted text in order to get each frequency from the known english frequencies
              char c = (char) ((str.charAt(j) - 'a' + shift) % 26 + 'a');

              //get english frequency from hashmap for the shifted letter
              Double val = encryptedFrequency.get(c);
              //
              double freq = (val*26+1)/26;
              //store the frequency in the hashmap
              encryptedFrequency.put(c, freq);
          }


          double x = 0;
          //compute the chi number for each letter of the english alphabet and the shifted letter
          for (char ch = 'a'; ch <= 'z'; ch++){
              x = x + ((Math.pow((encryptedFrequency.get(ch) + englishFrequency.get(ch)), 2)) / englishFrequency.get(ch));
          }

          chi[shift] = x;

          //empty the encryptedFrequency so its can be recomputed
          for (char ch = 'a'; ch <= 'z'; ++ch){
              encryptedFrequency.put(ch, 0.0);
          }
      }
  }
  //algorithm to find the smallest value in the array in order to find the most likely shift
  protected void findSmallest(){
      int i = 1;
      int shift = 0;
      while (i < chi.length){
          if (chi[i] < chi[shift]){
              shift = i;
          }
          i++;
      }
      decrypted = stringConstruct(shift);
  }
  //generate the english tranlation of the encrypted text given the shift found in the function above while preserving special chars like comma, full stop etc.
  public  String stringConstruct(int shift){
      String tempString = "";
      for (int j = 0; j < chars.length; j++){
          char ch = chars[j];
          if (ch == ' '){
              tempString = tempString + " ";
          }
          else if (ch == ','){
              tempString = tempString + ",";
          }
          else if (ch == '.'){
              tempString = tempString + ".";
          }
          else if (ch == 8217){
              tempString = tempString + "\'";
          }
          else{
              //shift the letter by the found shift
              if (Character.isUpperCase(ch)){
                  tempString = tempString + ((char) ((ch - 'A' + shift) % 26 + 'A'));
              }
              else {
                  tempString = tempString + ((char) ((ch - 'a' + shift) % 26 + 'a'));
              }
          }
      }
      return tempString;
  }

  protected void print(){
      System.out.println("==============================");
      System.out.println(encrypted);
      System.out.println("==============================");
      System.out.println(decrypted);
  }

  protected String getEncrypted(){
      return encrypted;
  }

  protected String getDecrypted(){
      return decrypted;
  }

  protected double[] getChi(){
      return chi;
  }
}
