import java.util.*;
import java.io.*;

public class TFIDF{
    public static ArrayList<HashMap<String, Integer>> wordList = new ArrayList<HashMap<String, Integer>> ();
    public static ArrayList<HashMap<String, Double>> list = new ArrayList<HashMap<String, Double>> ();
    public static HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
    public static ArrayList<HashMap<String, Double>> tfidf = new ArrayList<HashMap<String, Double>> ();
    public static int[] totalWords_multiple_docs;
    public static String[] publicArgs;

    public static void main(String[] args) {
        totalWords_multiple_docs = new int[args.length];
        publicArgs = args;

        File file;
        Scanner scanner;
        String line;
        HashMap<String,Integer> wc;

        for (int i = 0; i < args.length; i ++){
            wc = new HashMap<String,Integer>();
            file = new File(args[i]);
            try {
                scanner = new Scanner(file, "UTF-8");    //    file may or may not exist
                while(scanner.hasNextLine() )    {
                    line = scanner.nextLine();
                    line = line.toLowerCase();    // convert to lower case
                    String[] wordlist = line.split("[^\\p{L}0-9]+");    
                    for (String w : wordlist)    {
                        if ( w != null && !w.isEmpty()){
                            if ( wc.containsKey(w)){
                                wc.put(w, wc.get(w) + 1);    
                            }
                            else{
                                wc.put(w, 1);    
                            }                  
                        }         
                    }        
                }
                wordList.add(wc);
                
            }
            //    If the file doesn't exist, print out the error message.    
            catch (FileNotFoundException fnfe) {
                System.out.println(fnfe);
            }
        }
        if(args.length == 1){
            tf_single_doc();
        }
        else{
            multipleDocs();
        }
    }

    public static void multipleDocs(){
        tfidf();
    }

    public static void tfidf(){
        totalWords_multiple_docs();
        wordOccurance();
        for (int doc = 0; doc < publicArgs.length; doc++){
            HashMap<String, Double> tempHashMap = new HashMap<String, Double>();
            Iterator iterator = wordList.get(doc).entrySet().iterator();

            while (iterator.hasNext()){
                Map.Entry pair = (Map.Entry)iterator.next();
                double tf;
                double idf;

                tf = 1.0*((int)pair.getValue()) / totalWords_multiple_docs[doc];
                idf = Math.log10(1.0*publicArgs.length / (int)wordCount.get(pair.getKey()));
                tempHashMap.put((String)pair.getKey(), tf * idf);
            }
            tfidf.add(tempHashMap);
        }
        getMinKey_multiple_docs();
    }

    public static void tf_single_doc(){
        Iterator iterator = wordList.get(0).entrySet().iterator();
        HashMap<String, Double> tempHashMap = new HashMap<String, Double>();
        int totalWords = totalWords_single_doc();
        while (iterator.hasNext()){
            Map.Entry pair = (Map.Entry)iterator.next();
            double tf;

            tf =  1.0*((int)pair.getValue()) / totalWords;
            tempHashMap.put((String)pair.getKey(), tf);
        }
        list.add(tempHashMap);
        getMinKey_single_doc();
    }

    public static void wordOccurance(){
        for (int doc = 0; doc < publicArgs.length; doc++){
            for(String key : wordList.get(doc).keySet()) {
                if(wordCount.containsKey(key)){
                    wordCount.put(key, wordCount.get(key) + 1);
                }
                else{
                    wordCount.put(key, 1);
                }
            }
        }
    }
    
    public static int totalWords_single_doc(){
        int total = 0;
        for (String key: wordList.get(0).keySet()){
            total = total + wordList.get(0).get(key);
        }
        return total;
    }

    public static void totalWords_multiple_docs(){
        for(int doc = 0; doc < publicArgs.length; doc++){
            int total = 0;
            for (String key: wordList.get(doc).keySet()){
                total = total + wordList.get(doc).get(key);
            }
            totalWords_multiple_docs[doc] = total;
        }        
    }

    private static void getMinKey_single_doc() {
        String minKey = null;
        double minValue = Integer.MAX_VALUE;
        //double minValue = 0;
        for(String key : list.get(0).keySet()) {
            double value = list.get(0).get(key);
            if(value < minValue) {
                minValue = value;
                minKey = key;
            }
        }
        System.out.println("==========");
        System.out.println(publicArgs[0]);
        System.out.println("==========");
        System.out.println(minKey + " " + minValue);
    }

    private static void getMinKey_multiple_docs() {
        for(int doc = 0; doc < publicArgs.length; doc++){
            String minKey = null;
            double maxValue = 0;
            for(String key : tfidf.get(doc).keySet()) {
                double value = tfidf.get(doc).get(key);
                if(value > maxValue) {
                    maxValue = value;
                    minKey = key;
                }
            }
            System.out.println("==========");
            System.out.println(publicArgs[doc]);
            System.out.println("==========");
            System.out.println(minKey + " " + maxValue);
            System.out.println();
        }
    }
}