import java.util.*;
import java.io.*;

public class Main{
    //declaring variables for the program
    protected ArrayList<HashMap<String, Integer>> wordList = new ArrayList<HashMap<String, Integer>> ();
    protected HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
    protected ArrayList<HashMap<String, Double>> tfidf = new ArrayList<HashMap<String, Double>> ();
    protected int[] totalWords; //variable for storing number of total words for each document
    protected String[] fileArray; //variable for storing the arguments from the user

    public Main(String[] str){
        fileArray = str;
        //initialise the array with the number of files from the arguments
        totalWords = new int[fileArray.length];
        splitWords();
        tfidf();
    }


    //function to split each file into words
    protected void splitWords(){
        File file;
        Scanner scanner;
        String line;
        HashMap<String,Integer> wc;

        for (int i = 0; i < fileArray.length; i ++){
            wc = new HashMap<String,Integer>();
            file = new File(fileArray[i]);
            try {
                scanner = new Scanner(file, "UTF-8");    //    file may or may not exist
                while(scanner.hasNextLine() )    {
                    line = scanner.nextLine();
                    line = line.toLowerCase();    // convert to lower case
                    String[] wordlist = line.split("[^\\p{L}0-9]+");
                    for (String w : wordlist)    {
                        if ( w != null && !w.isEmpty()){
                            if ( wc.containsKey(w)){
                                wc.put(w, wc.get(w) + 1);
                            }
                            else{
                                wc.put(w, 1);
                            }
                        }
                    }
                }
                wordList.add(wc);
            }
            //    If the file doesn't exist, print out the error message.
            catch (FileNotFoundException fnfe) {
                System.out.println(fnfe);
            }
        }
    }
    //method to compute the TFIDF values when there are more than 1 files
    protected void tfidf(){
        //call a method to calculate total words for each document
        totalWords();
        //call method to calculate total occurances of every word in every document
        wordOccurance();

        for (int doc = 0; doc < fileArray.length; doc++){
            //creating an empty temporal hashmap to store values
            HashMap<String, Double> tempHashMap = new HashMap<String, Double>();

            Iterator iterator = wordList.get(doc).entrySet().iterator();
            //compute TF and IDF values for each word in the hashmap

            if(fileArray.length == 1){
                while (iterator.hasNext()){
                    Map.Entry pair = (Map.Entry)iterator.next();
                    double tf;

                    tf = 1.0*((int)pair.getValue()) / totalWords[doc];
                    tempHashMap.put((String)pair.getKey(), tf);
                }
            }

            else{
                while (iterator.hasNext()){
                    Map.Entry pair = (Map.Entry)iterator.next();
                    double tf;
                    double idf;

                    tf = 1.0*((int)pair.getValue()) / totalWords[doc];
                    idf = Math.log10(1.0*fileArray.length / (int)wordCount.get(pair.getKey()));
                    tempHashMap.put((String)pair.getKey(), tf * idf);
                }
            }
            //store all the data from the temporal hashmap to the final arraylist
            tfidf.add(tempHashMap);
        }
        //calling a function to print the most important word for each document
    }

    //count in how many documents the word exists
    protected void wordOccurance(){
        for (int doc = 0; doc < fileArray.length; doc++){
            for(String key : wordList.get(doc).keySet()) {
                //if words already exists in hashmap update value
                if(wordCount.containsKey(key)){
                    wordCount.put(key, wordCount.get(key) + 1);
                }
                //otherwise put the key-value pair in the hashmap
                else{
                    wordCount.put(key, 1);
                }
            }
        }
    }
    //compute the total number of words in each file
    protected void totalWords(){
        for(int doc = 0; doc < fileArray.length; doc++){
            int total = 0;
            for (String key: wordList.get(doc).keySet()){
                total = total + wordList.get(doc).get(key);
            }
            totalWords[doc] = total;
        }
    }

    //algorithm to find and print the most important word
    protected void printImportantWord() {
        //find the word with highest tfidf value
        System.out.println("\nMax TFIDF value for each file.\n");
        for(int doc = 0; doc < fileArray.length; doc++){
            String minKey = null;
            double maxValue = 0;
            for(String key : tfidf.get(doc).keySet()) {
                double value = tfidf.get(doc).get(key);
                if(value > maxValue) {
                    maxValue = value;
                    minKey = key;
                }
            }
            System.out.println("==========");
            System.out.println(fileArray[doc]);
            System.out.println("==========");
            System.out.println(minKey + " " + maxValue);
            System.out.println();
        }
    }

    protected ArrayList<HashMap<String, Double>> getTFIDF(){
        return tfidf;
    }

    protected ArrayList<HashMap<String, Integer>> getWordList(){
        return wordList;
    }

    protected HashMap<String, Integer> getWordCount(){
        return wordCount;
    }

    protected int[] getTotalWords(){
        return totalWords;
    }
}
